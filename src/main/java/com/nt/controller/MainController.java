package com.nt.controller;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
/*import org.springframework.util.SocketUtils;*/

import com.nt.service.EmployeeMgmtService;
import com.nt.vo.EmployeeVO;
/*import com.nt.bo.EmployeeBO;*/
import com.nt.dto.EmployeeDTO;

@SuppressWarnings("serial")
public class MainController implements Serializable {

	
	private EmployeeMgmtService service;
	
	public MainController(EmployeeMgmtService service)
	{
		this.service=service;
		
	}
	
	List<EmployeeDTO> listDTO =null;
	List<EmployeeVO> listVO = null;
	EmployeeVO vo= null;
	
	public List<EmployeeVO> gatherEmpsByDesg(String desg1, String desg2, String desg3) throws Exception
	{
		
		List<EmployeeDTO> listDTO=service.fetchEmployeeByDesgns(desg1, desg2, desg3);
		listVO =new ArrayList<EmployeeVO>();
		
		for(EmployeeDTO dto:listDTO)
		{
			vo=new EmployeeVO();
			BeanUtils.copyProperties(dto, vo);
			//System.out.println(toString());
			vo.setSerialNo(String.valueOf(dto.getSerialNo()));
			vo.setDeptNo(String.valueOf(dto.getDeptNo()));
			vo.setSal(String.valueOf(dto.getSal()));
			vo.setEmpno(String.valueOf(dto.getEmpno()));
			
			//convert list listDTO to listVO
		listVO.add(vo);
		}
		return listVO;
		
	}
}
