package com.nt.dto;

import java.io.Serializable;

public class EmployeeDTO implements Serializable{
	
	private Integer serialNo;
	private Integer empno;
	private String ename;
	private String job;
	private Integer sal;
	private Integer deptNo;
	
	
	public Integer getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}
	public Integer getEmpno() {
		return empno;
	}
	public void setEmpno(Integer empno) {
		this.empno = empno;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public Integer getSal() {
		return sal;
	}
	public void setSal(Integer sal) {
		this.sal = sal;
	}
	public Integer getDeptNo() {
		return deptNo;
	}
	public void setDeptNo(Integer deptNo) {
		this.deptNo = deptNo;
	}
	@Override
	public String toString() {
		return "EmployeeDTO [serialNo=" + serialNo + ", empno=" + empno
				+ ", ename=" + ename + ", job=" + job + ", sal=" + sal
				+ ", deptNo=" + deptNo + "]";
	}
	
	
}
