package com.nt.test;

import java.util.List;



import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.nt.controller.MainController;
import com.nt.vo.EmployeeVO;

public class NestedIOCContainer {

	public static void main(String[] args) {
		
		ApplicationContext parentContext,childContext=null;
		
		MainController controller = null;
		
		List<EmployeeVO> listVO = null;
	//create the Parent IOC Container	
	 parentContext = new ClassPathXmlApplicationContext("com/nt/cfgs/business-beans.xml");

	
     childContext =  new ClassPathXmlApplicationContext(new String[]{"com/nt/cfgs/presentation-beans.xml"},parentContext);

	
	//MainController Class Object
	
	 controller = childContext.getBean("controller",MainController.class);
	 			try 
					{
			        listVO=controller.gatherEmpsByDesg("clerk","manager","salesman");
			        //display the result
			        for(EmployeeVO vo : listVO)
			        {
			        	System.out.println(vo);
			        }
					}
					catch (Exception e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					((AbstractApplicationContext) parentContext).close();
					((AbstractApplicationContext) childContext).close();
	}//main close

}//class close
