package com.nt.bo;



public class EmployeeBO
{
	//wrapper variables default values is null so it will not allocate memory in heap.
	private Integer empno;
	private String ename;
	private Float sal;
	private String job;
	private Integer deptNo;
	
	
	
	public void setEmpno(Integer empno) {
		this.empno = empno;
	}
	public Integer getEmpno() {
		return empno;
	}
	
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEname() {
		return ename;
	}
	
	public void setSal(Float sal) {
		this.sal = sal;
	}
	public Float getSal() {
		return sal;
	}
	
	public void setJob(String job) {
		this.job = job;
	}
	
	public String getJob() {
		return job;
	}
	

	public void setDeptNo(Integer deptNo) {
		this.deptNo = deptNo;
	}
	
	public Integer getDeptNo() {
		return deptNo;
	}
	
	@Override
	public String toString() {
		return "EmployeeBO [empno=" + empno + ", ename=" + ename + ", sal="
				+ sal + ", job=" + job + ", deptNo=" + deptNo + "]";
	}
	
}