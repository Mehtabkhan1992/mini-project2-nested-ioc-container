package com.nt.vo;

public class EmployeeVO 
{
		private String serialNo;
		private String empno;
		private String ename;
		private String job;
		private String sal;
		private String deptNo;
		
		
		public void setSerialNo(String serialNo) {
			this.serialNo = serialNo;
		}
		public String getSerialNo() {
			return serialNo;
		}
		
		public void setEmpno(String empno) {
			this.empno = empno;
			
		}
		public String getEmpno() {
			return empno;
		}
		
		public void setEname(String ename) {
			this.ename = ename;
		}
		public String getEname() {
			return ename;
		}
		
		public void setJob(String job) {
			this.job = job;
		}

		public String getJob() {
			return job;
		}
		
		public void setSal(String sal) {
			this.sal = sal;
		}
		public String getSal() {
			return sal;
		}
		
		public void setDeptNo(String deptNo) {
			this.deptNo = deptNo;
		}
		public String getDeptNo() {
			return deptNo;
		}
		@Override
		public String toString() {
			return "EmployeeVO [serialNo=" + serialNo + ", empno=" + empno
					+ ", ename=" + ename + ", job=" + job + ", sal=" + sal
					+ ", deptNo=" + deptNo + "]";
		}
		
}
