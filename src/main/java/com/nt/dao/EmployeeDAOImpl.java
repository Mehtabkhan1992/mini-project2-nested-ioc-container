package com.nt.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.nt.bo.EmployeeBO;


public class EmployeeDAOImpl implements EmployeeDAO
{
	private static final String GET_EMP_DESGNS="select empno,ename,job,sal,deptNo from emp where job in(?,?,?)order by job";
	

	private DataSource ds;
	
	public EmployeeDAOImpl(DataSource ds)
	{
		this.ds=ds;
	}	
	
	@Override
	public List<EmployeeBO> getAllEmployeeByDesgs(String desg1, String desg2, String desg3) throws Exception 
	{
		
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs = null;
		List<EmployeeBO> listBO = null;
		
		
		try
		{
			
		//create the jdbc pooled Connection
		con=ds.getConnection();
		
		//creating the sql queries
		ps=con.prepareStatement(GET_EMP_DESGNS);
		ps.setString(1,desg1);
		ps.setString(2, desg2);
		ps.setString(3, desg3);
		
		rs=ps.executeQuery();
		
		listBO = new ArrayList();
		while(rs.next())
		{
	EmployeeBO bo= new EmployeeBO();
		
		bo.setEmpno(rs.getInt(1));
		bo.setEname(rs.getString(2));
		bo.setJob(rs.getString(3));
		bo.setSal(rs.getFloat(4));
		bo.setDeptNo(rs.getInt(5));
		// add the object into list object
		
		listBO.add(bo);			
		}//while block end
		}//try block
		catch(SQLException e)
		{
			e.printStackTrace();
			throw e;
		}//catch block
		
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}//unknown exception
		
		finally
		{
			
			try
			{
				if(rs!=null)
				{
					rs.close();
				}
			}
			catch(SQLException e)
			{
				e.printStackTrace();
				throw e;
			}
			try
			{
				if(ps!=null)
				{
					ps.close();
				}
			}
			catch(SQLException e)
			{
				e.printStackTrace();
				throw e;
			}
			try
			{
				if(con!=null)
				{
					con.close();
				}
			}
			catch(SQLException e)
			{
				e.printStackTrace();
				throw e;
			}
		}
		return listBO;
	}

}
