package com.nt.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.nt.bo.EmployeeBO;
import com.nt.dao.EmployeeDAO;
import com.nt.dto.EmployeeDTO;

public class EmployeeMgmtServiceImpl implements EmployeeMgmtService
{
	
	private EmployeeDAO dao;
	
	public EmployeeMgmtServiceImpl(EmployeeDAO dao)
	{
		this.dao=dao;
	}
	
	
	EmployeeDTO dto = null;
	

	public List<EmployeeDTO> fetchEmployeeByDesgns(String desg1, String desg2, String desg3) throws Exception {
		
		List<EmployeeBO> listBO =null;
		List<EmployeeDTO> listDTO=null;
		EmployeeDTO dto=null;
		//change the designation to UpperCase.
		desg1=desg1.toUpperCase();
		desg2=desg2.toUpperCase();
		desg3=desg3.toUpperCase();
		
		//use DAO
		listBO = dao.getAllEmployeeByDesgs(desg1,desg2,desg3);
		//code to convert ListDTO to listBO
		listDTO= new ArrayList<>();
		
		for(EmployeeBO bo:listBO)
		{
		dto = new EmployeeDTO();
		BeanUtils.copyProperties(bo, dto);
		dto.setSal(Math.round(bo.getSal()));
		dto.setSerialNo(listDTO.size()+1);
		listDTO.add(dto);
		}
		return listDTO;
		
		
		
		
	}//main close
}//class close
